import discord

from bot_config import BotConfig

botConfig = BotConfig('bot.json'

# move token to config
TOKEN = 'NjI3MjU0MTEzNzU4ODcxNTUy.XZWdTQ.Pkp696n1XskxTRGJsmBjfWq_--E'

client = discord.Client()

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    # Get 
    symbolLength = botConfig.commandSymbol.__len__()
    splitContent = message.content[symbolLength:].split( )
    command = splitContent.pop(0)
    params = splitContent

    if command in botConfig.commands:
        c = botConfig.commands[command]
        await botConfig.processCommand(c, params, message)

@client.event
async def on_ready():
    print(f'Logged in as {client.user.name} ({client.user.id})')

client.run(TOKEN)
