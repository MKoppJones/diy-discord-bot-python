import json
import http.client

def my_exec(code):
    exec('global i; i = %s' % code)
    global i
    return i

class BotConfig:
  rawConfig = None
  about = None
  startup = None
  commandSymbol = '!'
  commands = {}
  def __init__(self, filePath):
    with open('bot.json') as f:
      self.rawConfig = json.load(f)
    self.configure()
  
  def configure(self):
    self.about = About(self.rawConfig['about'])
    self.startup = Startup(self.rawConfig['startup'])
    self.commandSymbol = self.rawConfig['commandSymbol']
    for command in self.rawConfig['commands']:
      c = Command(command)
      self.commands[c.command] = c

  async def processCommand(self, command, params, message):
    # Does the command require any parameters
    if command.params > 0:
      # Have enough parameters been supplied
      if params.__len__() == command.params:
        if command.do != None:
          # Handle do
          res = command.do.exec(params)
          print(res)
          await message.channel.send(command.do.message.format(res))
        else:
          await message.channel.send(command.message.format(*params))
    else:
      # Still unhandled params?
      if params.__len__() > 0:
        nextCommand = params.pop(0)
        # Check if valid command
        if nextCommand in command.commands:
          await self.processCommand(command.commands[nextCommand], params, message)
        else:
          await message.channel.send(command.error)
      else:
        if command.do != None:
          # Handle do
          res = command.do.exec(params)
          await message.channel.send(command.do.message.format(res))
        else:
          await message.channel.send(command.message)

class About:
  name = 'DIY Dyno-Bot'
  author = ''
  description = ''
  version = ''
  def __init__(self, aboutJson):
    self.name = aboutJson['name']
    self.author = aboutJson['author']
    self.description = aboutJson['description']
    self.version = aboutJson['version']

class Startup:
  message = ''
  channel = ''
  def __init__(self, startupJson):
    self.message = startupJson['message']
    self.channel = startupJson['channel']

class Command:
  command = ''
  reply = False
  message = ''
  commands = {}
  params = 0
  do = None
  error = 'Something went wrong with the previous command'
  def __init__(self, json):
    self.command = json['command']

    if 'reply' in json:
      self.reply = json['reply']

    if 'message' in json:
      self.message = json['message']

    if 'commands' in json:
      for command in json['commands']:
        c = Command(command)
        self.commands[c.command] = c

    if 'params' in json:
      self.params = json['params']

    if 'error' in json:
      self.error = json['error']

    if 'do' in json:
      self.do = Function(json['do'])

global res

class Function:
  fnType = ''
  content = ''
  message = '{0}'
  def __init__(self, json):
    self.fnType = json['type']
    self.content = json['content']

    if 'message' in json:
      self.message = json['message']
      
  def exec(self, params):
    if self.fnType == 'get':
      return self.execGet(params)
    if self.fnType == 'exec':
      return self.execFn(params)
  
  def execGet(self, params):
    connection = http.client.HTTPSConnection(self.content)
    connection.request('GET', '/')
    res = connection.getresponse()
    return res.read()

  def execFn(self, params):
    return my_exec(self.content.format(*params))
